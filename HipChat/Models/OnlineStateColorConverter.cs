﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace HipChat
{
    public class OnlineStateColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var state = (OnlineState)value;

            switch (state)
            {
                case OnlineState.Online:
                    return new SolidColorBrush(Colors.Green);
                case OnlineState.Offline:
                    return new SolidColorBrush(Colors.White);
                case OnlineState.Away:
                    return new SolidColorBrush(Colors.Blue);
                case OnlineState.ExtendedAway:
                    return new SolidColorBrush(Colors.DarkGray);
                case OnlineState.DoNotDisturb:
                    return new SolidColorBrush(Colors.Red);
                case OnlineState.Chat:
                    return new SolidColorBrush(Colors.Yellow);
            }

            return new SolidColorBrush(Colors.LightGray);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
