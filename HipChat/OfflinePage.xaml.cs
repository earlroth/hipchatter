﻿using HipChat.Libs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace HipChat
{
    public sealed partial class OfflinePage : Page
    {
        public OfflinePage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            while (Frame.CanGoBack) Frame.BackStack.RemoveAt(0);

            GoogleAnalytics.EasyTracker.GetTracker().SendView("offline");
        }

        private void StatusSelect_Click(object sender, RoutedEventArgs e)
        {
            MenuFlyoutItem selectedItem = sender as MenuFlyoutItem;

            if (selectedItem != null)
            {
                Account.Current.OnlinePresence = selectedItem.Tag.ToString();
                Frame.Navigate(typeof(Login));
            }
        }
    }
}
